tetsumibfpy
===========

Nothing more than a compilator and interpreter for brainfuck.
The interpreter is a dirty piece of code relying on exec().

Compile brainfuck into

- C 
- Java 
- NASM 
- Scheme


interpreter usage:

    tetsumibfpy repl
    
compiler usage:

    tetsumibfpy [source_file] [output_file] [output_language]
    
eg: `tetsumibfpy hello.bf hello.java java`

[Note: I am no longer working on this.]
