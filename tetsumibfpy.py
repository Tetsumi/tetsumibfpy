# +----------------------------------------------------------------------------+
#
#	tetsumibfpy
#
#	A brainfuck compiler and interpreter.
#
#	22 Mar. 2013: Compiler to C
#       25 Mar. 2013: Compiler to Java
#       26 Mar. 2013: Compiler to Scheme
#       13 Apr. 2013: Compiler to NASM
#       21 Jun. 2013: REPL
#
# +----------------------------------------------------------------------------+
#
#	Copyright � 2013 Tetsumi <tetsumi@vmail.me>
#
#	This file is part of tetsumibfpy.
#
#	tetsumibfpy is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	tetsumibfpy is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with tetsumibfpy.  If not, see <http://www.gnu.org/licenses/>.
#
# +----------------------------------------------------------------------------+

__license__ = "GPLv3"
__version__ = "1.0"

import sys

OP_NONE = 0
OP_INC_POINTER = 1 # >
OP_DEC_POINTER = 2 # <
OP_INC_DATA = 3    # +
OP_DEC_DATA = 4    # -
OP_OUT_DATA = 5    # .
OP_IN_DATA = 6     # ,
OP_JUMPFOR = 7     # [
OP_JUMPBACK = 8    # ]
OP_END = 9

tokens = {'>': OP_INC_POINTER,
          '<': OP_DEC_POINTER,
          '+': OP_INC_DATA,
          '-': OP_DEC_DATA,
          '.': OP_OUT_DATA,
          ',': OP_IN_DATA,
          '[': OP_JUMPFOR,
          ']': OP_JUMPBACK}

def print_error (message):
    """Outputs message with the prefix 'ERROR:'."""
    print("ERROR:", message)
#------------------------------------------------------------------------------
def get_source (filename):
    """Opens file then reads the source code."""
    with open(filename, "r") as f:
        file_buffer = f.read()
    return file_buffer
#------------------------------------------------------------------------------
def parse (source):
    """Parse the source code into instructions."""
    instructions = []
    bracket_unmatch = 0
    for char in source:
        if char in "><+-.,p[]":
            instructions.append(tokens[char])
            if "[" == char:
                bracket_unmatch += 1
            elif "]" == char:
                if 0 == bracket_unmatch:
                    raise SyntaxError("] with no matching [.")
                bracket_unmatch -= 1
    if 0 < bracket_unmatch:
        raise SyntaxError("[ with no matching ].")
    return instructions
#------------------------------------------------------------------------------
def optimize (instructions):
    """Detects repeating instructions."""
    previous = OP_NONE
    same_amount = 0
    optimized = []
    cur_mem = 1
    for instruction in instructions:
        if instruction != previous and OP_NONE != previous:
            optimized.append((previous, same_amount))
            same_amount = 0
        if OP_END <= instruction:
            print_error("Unknown instruction.")
            sys.exit(1)
        previous = instruction
        same_amount += 1
    optimized.append((previous, same_amount))
    return optimized
#------------------------------------------------------------------------------
def output_c (filename, instructions, max_mem):
    """Translates instructions into C code and writes to output file."""
    with open(filename, 'w') as f:
        indentation = 1
        def write_indented(string):
            for x in range(indentation):
                f.write("    ")
            f.write(string)
        f.write("/* Generated from {0} with tetsumibfpy {1} */\n".format(sys.argv[1],
                                                                  __version__))
        f.write("#include <stdio.h>\n#include <stdint.h>\n")
        f.write("uint8_t memory[{0}];\n".format(max_mem))
        f.write("uint8_t *ptr = memory;\n")
        f.write("int main(void) {\n")
        for instruction in instructions:
            ins = instruction[0]
            arg = instruction[1]
            if OP_NONE == ins:
                continue
            elif OP_END <= ins:
                print_error("Unknown instruction. "+str(ins))
            if OP_INC_POINTER == ins:
                write_indented("ptr += {0};\n".format(arg))
            elif OP_DEC_POINTER == ins:
                write_indented("ptr -= {0};\n".format(arg))
            elif OP_INC_DATA == ins:
                write_indented("*ptr += {0};\n".format(arg))
            elif OP_DEC_DATA == ins:
                write_indented("*ptr -= {0};\n".format(arg))
            elif OP_OUT_DATA == ins:
                for x in range(arg):
                    write_indented("putchar(*ptr);\n")
            elif OP_IN_DATA == ins:
                write_indented("*ptr = getchar();\n")
            elif OP_JUMPFOR == ins:
                for x in range(arg):
                    write_indented("while(*ptr) {\n")
                    indentation += 1
            elif OP_JUMPBACK == ins:
                for x in range(arg):
                    indentation -= 1
                    write_indented("}\n")
        f.write("}\n")
#------------------------------------------------------------------------------
def output_java (filename, instructions, max_mem):
    """Translates instructions into Java code and writes to output file."""
    with open(filename, 'w') as f:
        indentation = 1
        def write_indented(string):
            for x in range(indentation):
                f.write("    ")
            f.write(string)
        f.write("// Generated from {0} with tetsumibfpy {1}\n".format(sys.argv[1],
                                                                 __version__))
        class_name = filename.rstrip(".java")
        f.write("public class {0} {{\n".format(class_name))
        write_indented("public static void main(String[] args) {\n")
        indentation += 1
        write_indented("int position = 0;\n")
        write_indented("char[] mem = new char[{0}];\n".format(max_mem))
        for instruction in instructions:
            ins = instruction[0]
            arg = instruction[1]
            if OP_NONE == ins:
                continue
            elif OP_END <= ins:
                print_error("Unknown instruction. "+str(ins))
            if OP_INC_POINTER == ins:
                write_indented("position += {0};\n".format(arg))
            elif OP_DEC_POINTER == ins:
                write_indented("position -= {0};\n".format(arg))
            elif OP_INC_DATA == ins:
                write_indented("mem[position] += {0};\n".format(arg))
            elif OP_DEC_DATA == ins:
                write_indented("mem[position] -= {0};\n".format(arg))
            elif OP_OUT_DATA == ins:
                for x in range(arg):
                    write_indented("System.out.print(mem[position]);\n")
            elif OP_IN_DATA == ins:
                write_indented("mem[position] = (char)System.in.read();\n")
            elif OP_JUMPFOR == ins:
                for x in range(arg):
                    write_indented("while(mem[position] > 0) {\n")
                    indentation += 1
            elif OP_JUMPBACK == ins:
                for x in range(arg):
                    indentation -= 1
                    write_indented("}\n")
        indentation -= 1
        write_indented("}\n")               
        f.write("}\n")
#------------------------------------------------------------------------------
def output_scheme (filename, instructions, max_mem):
    with open(filename, 'w') as f:
        indentation = 1
        def write_indented(string):
            for x in range(indentation):
                f.write("  ")
            f.write(string)
        f.write("; Generated from {0} with tetsumibfpy {1}\n".format(sys.argv[1],
                                                                 __version__))
        f.write("(define mem (make-vector {0} 0))\n".format(max_mem))
        f.write("(define pos 0)\n")
        for instruction in instructions:
            ins = instruction[0]
            arg = instruction[1]
            if OP_NONE == ins:
                continue
            elif OP_END <= ins:
                print_error("Unknown instruction. "+str(ins))
            if OP_INC_POINTER == ins:
                write_indented("(set! pos (+ pos {0}))\n".format(arg))
            elif OP_DEC_POINTER == ins:
                write_indented("(set! pos (- pos {0}))\n".format(arg))
            elif OP_INC_DATA == ins:
                write_indented("(vector-set! mem pos (+ (vector-ref mem pos) {0}))\n".format(arg))
            elif OP_DEC_DATA == ins:
                write_indented("(vector-set! mem pos (- (vector-ref mem pos) {0}))\n".format(arg))
            elif OP_OUT_DATA == ins:
                for x in range(arg):
                    write_indented("(display (integer->char (vector-ref mem pos)))\n")
            elif OP_IN_DATA == ins:
                write_indented("(vector-set! mem pos (read-char))\n")
            elif OP_JUMPFOR == ins:
                for x in range(arg):
                    write_indented("(while (> (vector-ref mem pos) 0)\n")
                    indentation += 1
            elif OP_JUMPBACK == ins:
                for x in range(arg):
                    indentation -= 1
                    write_indented(")\n")
#------------------------------------------------------------------------------
def output_nasm (filename, instructions, max_mem):
    with open(filename, 'w') as f:
        loop_stack = []
        loop_counter = 0
        f.write("; Generated from {0} with tetsumibfpy {1}\n".format(sys.argv[1],
                                                                 __version__))
        f.write("extern getchar\n")
        f.write("extern putchar\n")
        f.write("SECTION .bss\n")
        f.write("buffer: resb {0}\n".format(max_mem))
        f.write("SECTION .text\n")
        f.write("global main\n")
        f.write("main:\n")
        f.write("PUSH RBP\n")
        f.write("PUSH RBX\n")
        f.write("MOV RBP, RSP\n")
        f.write("SUB RSP, 16\n")
        f.write("MOV R12, buffer\n")
        for instruction in instructions:
            ins = instruction[0]
            arg = instruction[1]
            if OP_NONE == ins:
                continue
            elif OP_END <= ins:
                f.write("Unknown instruction. "+str(ins))
            if OP_INC_POINTER == ins:
                f.write("ADD R12, {0}\n".format(arg))
            elif OP_DEC_POINTER == ins:
                f.write("SUB R12, {0}\n".format(arg))
            elif OP_INC_DATA == ins:
                f.write("ADD byte [R12], {0}\n".format(arg))
            elif OP_DEC_DATA == ins:
                f.write("SUB byte [R12], {0}\n".format(arg))
            elif OP_OUT_DATA == ins:
                for x in range(arg):
                    f.write("MOV CL, byte [R12]\n")
                    f.write("CALL putchar\n")
            elif OP_IN_DATA == ins:
                f.write("CALL getchar\n")
                f.write("MOV byte [R12], AL\n")
            elif OP_JUMPFOR == ins:
                for x in range(arg):
                    loop_stack.append(loop_counter)
                    f.write("loop_{0}:\n".format(loop_counter))
                    f.write("CMP byte [R12], 0\n")
                    f.write("JE loop_end_{0}\n".format(loop_counter))
                    loop_counter += 1
            elif OP_JUMPBACK == ins:
                for x in range(arg):
                    loop_id = loop_stack.pop()
                    f.write("JMP loop_{0}\n".format(loop_id))
                    f.write("loop_end_{0}:\n".format(loop_id))
        f.write("ADD RSP, 16\n")
        f.write("POP RBX\n")
        f.write("POP RBP\n")
        f.write("XOR RAX,RAX\n")
        f.write("RET\n")
#------------------------------------------------------------------------------
def repl ():
    class State: # needed for exec().
        mem_pointer = 0
        mem_cells = [0] * 30000
    instructions = []
    running = True
    def cmd_commands(argv):
        print("Commands list:\n--------------")
        for key in sorted(commands):
            print(key, "->", commands[key][1])
    def cmd_exit(argv):
        nonlocal running
        running = False
        print("Bye!")
    def cmd_goto(argv):
        try:
            new_pointer = int(argv[1], 0)
        except IndexError as e:
            print_error("No argument.")
            return
        except ValueError as e:
            print_error("Argument is not a natural number.")
            return
        if 0 > new_pointer:
            print_error("New pointer is not a natural number.")
            return
        if 29999 < new_pointer:
            print_error("New pointer is outbound.")
            return
        State.mem_pointer = new_pointer
        print("Memory pointer now at", State.mem_pointer)
    def cmd_where(argv):
        print("Memory pointer at", State.mem_pointer)
    commands = {
        "commands": [cmd_commands, "Print list of commands."],
        "exit": [cmd_exit, "Quit tetsumibfpy."],
        "goto": [cmd_goto, "Set memory pointer, accept dec, hex, bin (eg: goto 0xFF)."],
        "where": [cmd_where, "Print value of the memory pointer."]
    }
    def isCode (line):
            if line[0] in "><+-[].,":
                return True
            else:
                return False
    def execute (instructions):
        nonlocal State
        indentation = 0
        source = ""
        def write_indented(str):
            nonlocal indentation
            nonlocal source
            for x in range(indentation):
                source += " "
            source += str
        for instruction in instructions:
            ins = instruction[0]
            arg = instruction[1]
            if OP_NONE == ins:
                continue
            elif OP_END <= ins:
                print_error("Unknown instruction. "+str(ins))
            if OP_INC_POINTER == ins:
                write_indented("State.mem_pointer += {0}\n".format(arg))
            elif OP_DEC_POINTER == ins:
                write_indented("State.mem_pointer -= {0}\n".format(arg))
            elif OP_INC_DATA == ins:
                write_indented("State.mem_cells[State.mem_pointer] += {0}\n".format(arg))
            elif OP_DEC_DATA == ins:
                write_indented("State.mem_cells[State.mem_pointer] -= {0}\n".format(arg))
            elif OP_OUT_DATA == ins:
                for x in range(arg):
                    write_indented("print(chr(State.mem_cells[State.mem_pointer]),end=\"\")\n")
            elif OP_IN_DATA == ins:
                write_indented("State.mem_cells[State.mem_pointer] = ord(input()[0])\n")
            elif OP_JUMPFOR == ins:
                for x in range(arg):
                    write_indented("while State.mem_cells[State.mem_pointer] > 0:\n")
                    indentation += 1
            elif OP_JUMPBACK == ins:
                for x in range(arg):
                    indentation -= 1
        exec(source,globals(),locals())
    print("Interpreter mode! Type 'commands' to print a list of commands.")
    while running:
        readline = input("--> ").lstrip().lower()
        if len(readline) == 0:
            continue
        if not isCode(readline):
            cmdline = readline.split()
            try:
                commands[cmdline[0]][0](cmdline)
            except KeyError as e:
                print("Unknown command:", cmdline[0])
            continue
        # Parsing loop for incomplete loop.
 
        try:
            ins = parse(readline)
        except SyntaxError as e:
            print_error(e)
            continue
        ins = optimize(ins)
        print(ins)
        execute(ins)
#------------------------------------------------------------------------------
output_functions = {"c": output_c,
                    "java": output_java,
                    "scheme": output_scheme,
                    "nasm": output_nasm}

if __name__ == "__main__":
    print("tetsumibfpy", __version__)
    len_args = len(sys.argv)
    if 2 > len_args:
        print("interpreter usage:")
        print("    tetsumibfpy repl\n")
        print("compiler usage:")
        print("    tetsumibfpy [source_file] [output_file] [output_language]")
        print("      eg: tetsumibfpy hello.bf hello.java java\n")
        print("available output languages:")
        for key in output_functions.keys():
            print("    ->", key.title())
        sys.exit(0)
    if sys.argv[1].lower() == "repl":
        repl()
        sys.exit(0)
    if 4 > len_args:
        print_error("Not enough arguments.")
        sys.exit(1)
    if 5 < len_args:
        print_error("Too many arguments.")
        sys.exit(1)
    source = get_source(sys.argv[1])
    if len(source) < 1:
        print_error("Source is empty.")
        sys.exit(1)
    output_lang = sys.argv[3].lower()
    if output_lang not in output_functions:
        print_error("Output language is not supported.")
        sys.exit(1)
    print("Output language:", output_lang)
    print("Compiling", sys.argv[1], "into", sys.argv[2])
    try:
        instructions = parse(source)
    except SyntaxError as e:
        print_error(e)
        sys.exit(0)
    optimized = optimize(instructions)
    max_mem = 30000 # standard size.
    output_functions[output_lang](sys.argv[2], optimized, max_mem)  
    print("Done!")
